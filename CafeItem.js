import React from 'react';
import { Text, View, Image, StyleSheet } from 'react-native';

class CafeItem extends React.Component {
    state = {
        uri: 'https://juicey.net/images/' + this.props.logo
    }
    render(){
        return (
            <View style={{ flex: 1, flexDirection: 'row',  padding: 10, borderTopWidth: 1 }}>
                <Image source={{ uri: this.state.uri }} style={{ height: 100, flex: 1 }}/>
                <View style={{ flex: 2, marginHorizontal: 10, alignContent: 'center' }}>
                    <Text style={{fontSize: 20, color: "#000" }}>{this.props.translations[1].name}</Text>
                    <Text>Pay by: {this.props.cod == '1' ? 'Cash': ''} { this.props.cod == '1' && this.props.credit_card == '1' ? ',': '' } {this.props.credit_card == '1' ? 'Credit card': ''}</Text>
                    <Text>Avg: {this.props.delivery_time} min</Text>
                    <Text>Min: {this.props.minimum_order_amount} USD</Text>
                    <Text>Delivery: {this.props.delivery_fee} USD</Text>
                </View>
            </View>
        );
    }
}

export default CafeItem;