import React from 'react';
import { FlatList, View, StyleSheet, TextInput, Button, ActivityIndicator, Text } from 'react-native';
import CafeItem from './CafeItem';
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        data: null,
        isLoaded: false,
      }
  }
  componentDidMount = () => {
    fetch('https://juicey.net/api/cafes')
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          data: responseJson.data,
          isLoaded: true
        });
      })
      .catch((error) => {
        console.error(error);
      });
  }
  render() {
    return (
      <View style={styles.container}>
        { this.state.isLoaded == true ? (
          <View>
            <View style={styles.header}>
              <TextInput
                style={styles.searchInput}
                placeholder="Search cafe"
                placeholderTextColor="#000"
              />
              <Button title="Ok" style={styles.cancel} color="orange" />
              <Button title="Cancel" style={styles.cancel} color="orange" />
            </View> 
            <View>
              <Text style={styles.mainText}>Our cafes</Text>
              <FlatList style={{ marginBottom: 50 }} data={this.state.data} keyExtractor={(item, index) => item.id.toString()} renderItem={ ({item}) => (<CafeItem {...item}/>) } />
            </View>
          </View>
        )
        : 
        <ActivityIndicator size="large" color="#0000ff" />
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ecf0f1'
  },
  header: {
    backgroundColor: 'orange',
    flexDirection: 'row',
    height: 50
  },
  searchInput: {
    backgroundColor: '#fff',
    flex: 1,
  },
  cancel: {
    height: 50,
    flex: 1
  },
  mainText: {
    fontSize: 30,
    textAlign: "center",
    color: "#000",
  }
});

export default App;